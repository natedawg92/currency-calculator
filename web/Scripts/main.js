$(function () {
	//Global Variables
	currencyCalculator = false;
	base = "";
	baseValue = 0;
	rates = [];
	currentCurrency = "";
	var a = ["£","€","Zł","kr"];
	
	//get Conversion data from yahoo finance API
	var jqxhr = $.ajax({ 
        url: 'http://query.yahooapis.com/v1/public/yql',
        dataType: 'jsonp',
        data: {
            q :         'select * from yahoo.finance.xchange where pair="GBPEUR, GBPPLN, GBPDKK, EURGBP, EURPLN, EURDKK, PLNGBP, PLNEUR, PLNDKK, DKKGBP, DKKEUR, DKKPLN"',
            format :    'json',
            env :       'store://datatables.org/alltableswithkeys'
        },
		timeout : 1000
    })
	.done(function( data ){
		var items = data.query.results.rate;
		items.forEach(function(x){
			var b = {id: x.id, rate: x.Rate};
			rates.push(b);
		});
		rates.push({id:"GBPGBP", rate: 1});
		rates.push({id:"EUREUR", rate: 1});
		rates.push({id:"PLNPLN", rate: 1});
		rates.push({id:"DKKDKK", rate: 1});
		localStorage.setItem('ratesStorage', JSON.stringify(rates));
	})
	.fail(function()  {
		storage = JSON.parse(localStorage.getItem('ratesStorage'));
		if (storage === null){
			alert("No Backup for conversion rates! \n please connect to the internet to generate a backup file automatically. \n The Calculator will continue to work as normal however the conversion functions will be unavailable.");
			a.forEach(function(b){document.getElementById(b).disabled = true;}); 
		} else {
			rates = storage;
		}
	});
	
	//Get Index of ID
	function arrayObjectIndexOf(searchTerm) {
		for(var i = 0, len = rates.length; i < len; i++) {
			if (rates[i].id === searchTerm) return i;
		}
		// -1 = Not Found
		return -1;
	}
	
	function changeCurrency(id){
		currencyCalculator = true;
		switch(id){
			case "£" : currentCurrency = "GBP"; break;
			case "€" : currentCurrency = "EUR"; break;
			case "Zł" : currentCurrency = "PLN"; break;
			case "kr" : currentCurrency = "DKK"; break;
			default : currentCurrency = "ERROR";
		}
		if(Number(baseValue) == 0){baseValue = $input.val()}
		a.forEach(function(b) {
			if (b == id) { 
				document.getElementById(b).disabled = true; 
			} else { 
				document.getElementById(b).disabled = false;
			}
		});
		document.getElementById("symbol").innerHTML = id;
		if(base === "" || Number($input.val()) === 0){
			base = currentCurrency;
		}
		x = base+currentCurrency;
		if(Number($input.val() != 0)){
			convert = Number(baseValue)*rates[arrayObjectIndexOf(x)].rate;
			if(convert != 0){
				$input.val(convert);
			}
		}
		$input.val(Number($input.val()).toFixed(2));
	}
	
	function clearCurrency(){
		a.forEach(function(b) {document.getElementById(b).disabled = false;});
		document.getElementById("symbol").innerHTML = "";
		currentCurrency = ""
		baseValue = 0;
		base = "";
	}
	
	// Add containers to the DOM
	var $calc = document.getElementById("calc");
	var $calculator = $('<div/>', {id: 'calculator'}).appendTo($calc);
	var $symbol = $('<div/>', {id: 'symbol'}).appendTo($calculator);
	var $input = $('<input/>', {id: 'input'}).appendTo($calculator);
	var $buttons = $('<div/>', {id: 'buttons'}).appendTo($calculator);
	// Add buttons to the DOM
	$.each('£ England,€ Europe,Zł Poland,kr Denmark,Clear,←,7,8,9,÷,4,5,6,X,1,2,3,-,.,0,=,+'.split(','), function () {
		var $button = $('<button/>', {
			id: this.toString().split(' ')[0], 
			text: this.toString(), 
			click: function () {
				// Handle button clicks
				switch ($(this).text()) {
					// '=' will fetch the current expression string, evaluate it,
					// and write the result back into the input/output field.
					// That's where the actual calculation happens. 
				case '=': 
						if(!currencyCalculator){
							try {$input.val(eval($input.val()));} catch (e) {$input.val('ERROR');}
						} else {
							try {$input.val(eval($input.val()).toFixed(2));} catch (e) {$input.val('ERROR');}
						}
						baseValue = $input.val();
						base = currentCurrency;
					// 'Clear' will clear the input/output field
					break; case 'Clear':
						clearCurrency();
						currencyCalculator = false;
						return $input.val('');
					// '←' will delete the last character from the input/output field
					break; case '←': 
						baseValue = $input.val();
						base = currentCurrency;
						return $input.val($input.val().replace(/.$/, ''));
					//convert current value from base currency to GBP
					break; case '£ England': 
						id = base+"GBP";
						changeCurrency("£");
					//convert current value from base currency to EUR
					break; case '€ Europe': 
						id = base+"EUR";
						changeCurrency("€");
					//convert current value from base currency to PLN
					break; case 'Zł Poland':
						changeCurrency("Zł");
					//convert current value from base currency to DKK
					break; case 'kr Denmark': 
						changeCurrency("kr");
					// All other buttons will add a character to the input/output field
					break; case '÷' : $input.val($input.val() + '/');
					break; case 'X' : $input.val($input.val() + '*');
					break; default: if(Number($input.val())== 0){$input.val("");}
						$input.val($input.val() + $(this).text()); 
				}
			},
		}).appendTo($buttons);
	});
	FastClick.attach(document.body);
});

